package mcib3d.tapas.ext.CLIJ.core;

import mcib3d.tapas.core.TapasProcessingAbstract;
import net.haesleinhuepf.clij.clearcl.ClearCLBuffer;

public interface TapasProcessingCLIJ extends TapasProcessingAbstract<ClearCLBuffer> {

}
